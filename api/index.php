<?php
 
require 'vendor/autoload.php';
$app = new \Slim\Slim();

function getDB()
{
    $dbhost = "localhost";
    $dbuser = "application";
    $dbpass = "application";
    $dbname = "passgen";
 
    $mysql_conn_string = "mysql:host=$dbhost;dbname=$dbname";
    $dbConnection = new PDO($mysql_conn_string, $dbuser, $dbpass); 
    $dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $dbConnection;
};

$app->get('/', function() use($app) {
    $app->response->setStatus(200);
    echo "Welcome to Slim based API";
});

$app->get('/count', function () use ($app) {
    try 
    {
        $db = getDB();
 
        $sth = $db->prepare("SELECT COUNT(*) as cnt FROM Password");
        $sth->execute();
        $count = $sth->fetch(PDO::FETCH_OBJ);
 
        $app->response->setStatus(200);
        $app->response()->headers->set('Content-Type', 'application/json');
        $response['status'] = 'SUCCESS';
        $response['count'] = $count->cnt;
        echo json_encode($response);
        $db = null;
 
    } catch(PDOException $e) {
        $app->response()->setStatus(404);
        echo json_encode(array("status" => "ERROR", "code" => 0, "message" => $e->getMessage()));
    }
});

$app->post('/generate', function () use ($app) {
    $session = $app->request->params('session');
    $length = intval($app->request->params('passlength'));
    $isUppercase = $app->request->params('uppercase');
    $isNumeric = $app->request->params('numeric');
    $isSymbol = $app->request->params('symbol');

    $upperCases = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $lowerCases = "abcdefghijklmnopqrstuvwxyz";
    $numeric = "1234567890";
    $symbols = "!#%&()*+,-./~:;=?@[]^_`{|}";

    $upperCase = str_split($upperCases);
    $lowerCase = str_split($lowerCases);
    $number = str_split($numeric);
    $symbol = str_split($symbols);

    $finalChar = $lowerCase;

    if ($isUppercase == 1) {
        $finalChar = array_merge($finalChar, $upperCase);
    }

    if ($isNumeric == 1) {
        $finalChar = array_merge($finalChar, $number);
    }

    if ($isSymbol == 1) {
        $finalChar = array_merge($finalChar, $symbol);
    }

    $passString = "";
    for ($i = 0; $i < $length; $i++) {
        $rand = rand(0,count($finalChar)-1);
        $passString .= $finalChar[$rand];
    }

    $passString;

    try 
    {
        $db = getDB();
 
        $sth = $db->prepare("INSERT Password(ip,password) 
            VALUES (:ip, :password)");
 
        $sth->bindParam(':ip', $session, PDO::PARAM_INT);
        $sth->bindParam(':password', $passString, PDO::PARAM_INT);
        $sth->execute();
 
        $app->response->setStatus(200);
        $app->response()->headers->set('Content-Type', 'application/json');
        $response['status'] = 'SUCCESS';
        $response['password'] = $passString;
        echo json_encode($response);
        $db = null;
 
    } catch(PDOException $e) {
        $app->response()->setStatus(404);
        echo json_encode(array("status" => "ERROR", "code" => 0, "message" => $e->getMessage()));
    }
});
 
$app->run();

?>