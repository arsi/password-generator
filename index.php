<?php  
	$passLength = 10;
	$defaultUpper = "unchecked";
	$defaultSymbol = "unchecked";
	$defaultNumber = "unchecked";

	$countJson = httpGet("http://localhost/passgen/api/count");

	$count = json_decode($countJson)->count;

	function httpGet($url)
	{
	    $ch = curl_init();  
	 
	    curl_setopt($ch,CURLOPT_URL,$url);
	    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
	 
	    $output=curl_exec($ch);
	 
	    curl_close($ch);
	    return $output;
	}

	function get_client_ip() {
	    $ipaddress = '';
	    if (isset($_SERVER['HTTP_CLIENT_IP']))
	        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
	    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
	        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
	    else if(isset($_SERVER['HTTP_X_FORWARDED']))
	        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
	    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
	        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
	    else if(isset($_SERVER['HTTP_FORWARDED']))
	        $ipaddress = $_SERVER['HTTP_FORWARDED'];
	    else if(isset($_SERVER['REMOTE_ADDR']))
	        $ipaddress = $_SERVER['REMOTE_ADDR'];
	    else
	        $ipaddress = 'UNKNOWN';
	    return $ipaddress;
	}

	if ($_SERVER['REQUEST_METHOD'] === 'POST') {
		$passLength = $_POST['length'];
		$useUppercase = $_POST['uppercase'];
		$useSymbol = $_POST['symbol'];
		$useNumber = $_POST['number'];

		$ch = curl_init();

		$uppercase = 0;
		$symbol = 0;
		$number = 0;

		$session;

		if ($useUppercase) {
			$uppercase = 1;
			$defaultUpper = "checked";
		}

		if ($useSymbol) {
			$symbol = 1;
			$defaultSymbol = "checked";
		}

		if ($useNumber) {
			$number = 1;
			$defaultNumber = "checked";
		}

		curl_setopt($ch, CURLOPT_URL,"http://localhost/passgen/api/generate");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, 
		http_build_query(array('session' => get_client_ip(),'passlength' => $passLength, 'uppercase' => $uppercase, 'numeric' => $number, 'symbol' => $symbol)));

		$server_output = curl_exec ($ch);

		curl_close ($ch);

		$output = json_decode($server_output);

		if ($output->status = 'SUCCESS') {
			$passString = $output->password;
		}
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Safe Password Generator</title>
	<!--Import Google Icon Font-->
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
	<link type="text/css" rel="stylesheet" href="css/style.css"/>
	<link rel="icon" type="image/ico" href="favicon-rebel.ico" />
	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
	<!--Import jQuery before materialize.js-->
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script type="text/javascript" src="js/materialize.min.js"></script>
	<script src="js/clipboard.js-master/dist/clipboard.min.js"></script>
	<script type="text/javascript">
		var clipboard = new Clipboard('.btn1');
	</script>
	<nav class="light-blue darken-2">
		<div class="nav-wrapper">
			<a href="" class="brand-logo center logo logo1">PASSWORD<span class="logo logo2">GENERATOR</span></a>
		</div>
	</nav>

	<div class="row content">
		<div class="col l4 offset-l4">
			<h5 class="center-align title">GENERATE MY SAFE PASSWORD</h5>
			<div class="card">
				<div class="card-content">
					<form action="" method="post">
						<div class="row">
							<label for="filled-in-box1">Password Length</label>
							<p class="range-field">
								<input value="<?php echo $passLength; ?>" type="range" name="length" id="test5" min="8" max="64" />
							</p>
						</div>
						<div class="row">
							<div class="inp col s4 center-align">
								<label>Include Uppercase</label>
								<div class="switch">
								    <label>
								      No
								      <input <?php echo $defaultUpper;?> name="uppercase" type="checkbox">
								      <span class="lever"></span>
								      Yes
								    </label>
								</div>
							</div>
							<div class="inp col s4 center-align">
								<label>Include Symbol</label>
								<div class="switch">
								    <label>
								      No
								      <input <?php echo $defaultSymbol;?> name="symbol" type="checkbox">
								      <span class="lever"></span>
								      Yes
								    </label>
								</div>
							</div>
							<div class="inp col s4 center-align">
								<label>Include Number</label>
								<div class="switch">
								    <label>
								      No
								      <input <?php echo $defaultNumber;?> name="number" type="checkbox">
								      <span class="lever"></span>
								      Yes
								    </label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col s12 center-align">
								<button id="submit_form" onclick="form_submit()" data-target="modal" class="btn waves-effect waves-light light-blue darken-2" type="submit" name="action">GENERATE
									<i class="material-icons right">toys</i>
								</button>
							</div>
						</div>
						<div class="row generate center-align">
							<label>Version 1.0.0</label><br>	
							<label>Total passwords generated: <?php echo $count; ?></label>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<?php  
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			echo "<div class='row center-align'><div class='col l10 offset-l1'><div class='card'><div class='card-content'>";
			echo "<h5 id='pass'>".$passString."</h5>";
			echo "<button onclick=\"Materialize.toast('Copied', 4000)\" class=\"btn btn1 light-blue darken-2\" data-clipboard-target=\"#pass\"><img src=\"assets/clippy.svg\" alt=\"Copy\"><i class='material-icons right'>content_copy</i></button>";
			echo "</div></div></div></div>";
		}
	?>	
</body>
</html>